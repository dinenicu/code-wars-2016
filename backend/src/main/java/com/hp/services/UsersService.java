package com.hp.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.entity.User;
import com.hp.entity.Wishlist;
import com.hp.utils.Util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UsersService {
    private final static Logger LOGGER = Logger.getLogger(UsersService.class.getName());

    private Map<Integer, User> usersById;
    private Map<Integer, Wishlist> wishlistsById;

    public UsersService() {
        wishlistsById = new HashMap<>();
        usersById = new HashMap<>();
        try {
            loadUsers();
        } catch (URISyntaxException | IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public User login(String username, String password) {
        return getUserByUsernamePassword(username, password);
    }

    public boolean authenticate(String authCredentials) {
        if (null == authCredentials)
            return false;
        // header value format will be "Basic encodedstring" for Basic
        // authentication. Example "Basic YWRtaW46YWRtaW4="
        final String encodedUserPassword = authCredentials.replaceFirst("Basic" + " ", "");
        String usernameAndPassword;
        try {
            byte[] decodedBytes = Base64.getDecoder().decode(
                    encodedUserPassword);
            usernameAndPassword = new String(decodedBytes, "UTF-8");
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return false;
        }
        final StringTokenizer tokenizer = new StringTokenizer(
                usernameAndPassword, ":");
        final String username = tokenizer.nextToken();
        final String password = tokenizer.nextToken();

        return getUserByUsernamePassword(username, password) != null;
    }

    public User addUser(User user) {
        if (!user.getFirstName().trim().isEmpty() && !user.getLastName().trim().isEmpty() &&
                !user.getEmail().trim().isEmpty() && !user.getUsername().trim().isEmpty() &&
                !user.getPassword().trim().isEmpty()) {
            user.setId(usersById.size());
            usersById.put(user.getId(), user);
            return user;
        }
        return null;
    }

    public boolean updateUser(int id, User updatedUser) {
        if (usersById.get(id) == null) {
            return false;
        }
        usersById.put(id, updatedUser);
        return true;
    }

    public boolean deleteUser(int id) {
        if (usersById.get(id) == null) {
            return false;
        }
        usersById.remove(id);
        return true;
    }

    public User getUserById(int id) {
        return usersById.get(id);
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(usersById.values());
    }

    private User getUserByUsernamePassword(String username, String password) {
        for (Map.Entry<Integer, User> entry : usersById.entrySet()) {
            if (entry.getValue().getUsername().equals(username) &&
                    entry.getValue().getPassword().equals(Util.encryptUsingMD5(password))) {
                return entry.getValue();
            }
        }
        return null;
    }

    public List<Wishlist> getUserWishlists(int userId) {
        User user = usersById.get(userId);

        if (user != null) {
            return user.getWishlists();
        }
        return null;
    }

    public Wishlist addUserWishlist(int userId, Wishlist wishlist) {
        User user = usersById.get(userId);

        wishlist.setId(wishlistsById.size() + 1);
        user.getWishlists().add(wishlist);
        wishlistsById.put(wishlist.getId(), wishlist);
        return wishlist;
    }

    public boolean removeUserWishlist(int userId, int wishlistId) {
        User user = usersById.get(userId);

        for (Wishlist wishlist : user.getWishlists()) {
            if (wishlist.getId() == wishlistId) {
                user.getWishlists().remove(wishlist);
                wishlistsById.remove(wishlistId);
                return true;
            }
        }
        return false;
    }

    public void updateUserWishlist(int userId, Wishlist newWishlist) {
        User user = usersById.get(userId);

        for (Wishlist wishlist : user.getWishlists()) {
            if (wishlist.getId() == newWishlist.getId()) {
                wishlist.setName(newWishlist.getName());
                wishlist.setProducts(newWishlist.getProducts());
            }
        }
    }

    private void loadUsers() throws URISyntaxException, IOException {
        final ObjectMapper mapper = new ObjectMapper();

        String usersJson = new String(Files.readAllBytes(Util.getPath("users/users.json")), "utf-8");
        User[] usersFile = mapper.readValue(usersJson, User[].class);

        for (User user : usersFile) {
            user.setId(usersById.size());
            user.setPassword(Util.encryptUsingMD5(user.getPassword()));
            usersById.put(user.getId(), user);
        }
    }
}
