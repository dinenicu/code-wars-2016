package com.hp.utils;

import com.hp.services.UsersService;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.List;

public class AuthenticationFilter implements ContainerResponseFilter {
    public static final String AUTHENTICATION_HEADER = "Authorization";
    public static final int SC_UNAUTHORIZED = 401;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {

        MultivaluedMap<String, String> headers = requestContext.getHeaders();
        List<String> authCredentials = headers.get(AUTHENTICATION_HEADER);

        if (requestContext.getMethod().equals("OPTIONS")) {
            return;
        }

        if (authCredentials != null && !authCredentials.isEmpty()) {
            String credentials = authCredentials.get(0);
            UsersService usersService = new UsersService();
            boolean authenticationStatus = usersService.authenticate(credentials);
            if (!authenticationStatus) {
                responseContext.setStatus(SC_UNAUTHORIZED);
                // make sure nothing is sent
                responseContext.setEntity(null);
            }
        } else {
            responseContext.setStatus(SC_UNAUTHORIZED);
            // make sure nothing is sent
            responseContext.setEntity(null);
        }
    }
}
