'use strict';

angular.module('codewars2016App')
    .controller('EditWishlistCtrl', ['$scope', 'wishlistService', 'userService', '$q', '$uibModalInstance',
        function ($scope, wishlistService, userService, $q, $uibModalInstance, wishlist) {
            $scope.wishlist = wishlist;

            $scope.removeProduct = function (index) {
                $scope.wishlist.products.splice(index,1);
            };

            $scope.editWishlist = function () {
                wishlistService.updateWishList(userService.user.id, $scope.wishlist).then(function () {
                        $uibModalInstance.close();
                    });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
    }]);
