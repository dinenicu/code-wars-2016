'use strict';

angular.module('codewars2016App')
    .controller('NewWishlistCtrl', ['$scope', 'wishlistService', 'userService', '$q', '$uibModalInstance',
        function ($scope, wishlistService, userService, $q, $uibModalInstance) {
            $scope.wishlist = {
                name: ''
            };

            $scope.addWishlist = function () {
                wishlistService.addWishList(userService.user.id, $scope.wishlist).then(function () {
                        $uibModalInstance.close();
                    });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
    }]);
