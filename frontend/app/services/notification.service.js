'use strict';

angular.module('codewars.services')
  .service('notificationService', function () {
    var uuid = 0,
      events = {};

    this.subscribe = function (subscriptionEvent, callback) {
      var subscribed = events[subscriptionEvent];
      if (!subscribed) {
        subscribed = events[subscriptionEvent] = [];
      }

      uuid++;

      subscribed.push({
        uuid: uuid,
        callback: callback
      });
      return uuid;
    };

    this.unsubscribe = function (event, uuid) {
      var subscribed = events[event],
        index;
      if (!!subscribed) {
        for (index = 0; index < subscribed.length; index++) {
          if (subscribed[index].uuid === uuid) {
            subscribed.splice(index, 1);
          }
        }
      }
    };

    this.notify = function (event) {
      var subscribed = events[event];
      angular.forEach(subscribed, function (subscribedItem) {
        subscribedItem.callback();
      });
    };
  });
